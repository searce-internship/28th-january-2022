#include <iostream>

using namespace std;

int stack[5];
int top = -1;

void pop()
{
	if (top == -1)
		cout << "Enpty stack" << "\n";
	else
	{
		cout << stack[top] << "\n";
		top--;
	}
}

void push(int data)
{
	if (top == 5)
	{
		cout << "Overflow" << "\n";
	}
	else
	{
		top++;
		stack[top] = data;
	}
}

void peek()
{
	cout << stack[top] << "\n";
}

void display()
{
	int dis=0;
	while (dis != top+1)
	{
		cout << stack[dis] << "\n";
		dis++;
	}
}

int main()
{
	pop(); //empty stack
	push(1); 
	push(2);
	pop(); //2
	push(2);
	push(3);
	push(4);
	push(5);
	push(6);
	push(7); //overflow
	peek(); //5
	display(); // 1 2 3 4 5
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
