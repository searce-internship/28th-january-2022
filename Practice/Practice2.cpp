#include <iostream>
#include <cstring>
#include<string>

using namespace std;

struct node
{
	int data;
	struct node* next;
	node(int data)
	{
		this->data = data;
		next = NULL;
	}
};
node* head = NULL;
node* last;
node* iti;
struct linkedlist
{
	void display()
	{
		last = head;
		while (last != NULL)
		{
			cout << last->data << "\t";
			last = last->next;
		}
	}

	void insertbeg(int data)
	{
		node* temp = new node(data);
		if (head == NULL)
		{
			head = temp;
			last = head;
		}
		else
		{
			temp->next = head;
			head = temp;
		}
	}

	void insertmid(int data, int pos)
	{
		if (head == NULL)
		{
			return;
		}
		else
		{
			int i=0;
			last = head;
			while (i < (pos-1))
			{
				i++;
				last = last->next;
			}
			node* temp;
			temp = last->next;
			node* in = new node(data);
			last->next = in;
			in->next = temp;
		}
	}

	void insertlast(int data)
	{
		node* temp = new node(data);
		last = head;
		while (last->next != NULL)
		{
			last = last->next;
		}
		last->next = temp;
		last = temp;
	}

	void deletestart()
	{
		if (head != NULL)
		{
			head = head->next;
		}
		else
		{
			head = NULL;
		}
	}

	void deleteend()
	{
		last = head;
		while (last->next->next != NULL)
		{
			last = last->next;
		}
		last->next = NULL;
	}

	int deletemid(int p)
	{
		last = head;
		int flag=0;
		while (last->next != NULL)
		{
			if (last->next->data == p)
			{
				last->next = last->next->next;
				flag++;
				break;
			}
			else
				last = last->next;
		}
		return flag;
	}

	int length()
	{
		last = head;
		int count=0;
		while (last != NULL)
		{
			count++;
			last = last->next;
		}
		return count;
	}

	void reverse()
	{
		//last = head;
		node* current;
		node* prev;
		node* agla;
		current = head;
		prev = head;
		agla = head->next;
		while (agla != NULL)
		{
			//agla = last->next;
			if (current == head)
			{
				current->next = NULL;
			}
			current = agla;
			agla = agla->next;
			current->next = prev;
			prev = current;
		}
		head = current;
	}

	void push(int data)
	{
		node* temp = new node(data);
		if (head == NULL)
		{
			head = temp;
			last = head;
		}
		else
		{
			last->next = temp;
			last = temp;
		}
	}
};

int main()
{
	linkedlist ll;
	ll.push(1);
	ll.push(2);
	ll.push(3);
	ll.push(4);
	ll.push(5);
	ll.display();
	cout << "\n";
	ll.reverse();
	ll.display();
	cout << "\n";
	/*ll.insertbeg(0);
	ll.display();
	cout << "\n";
	ll.insertlast(6);
	ll.display();
	cout << "\n";
	ll.insertmid(100, 3);
	ll.display();
	cout << "\n";
	ll.deletestart();
	ll.display();
	cout << "\n";
	ll.deleteend();
	ll.display();
	cout << "\n";
	int a;
	a = ll.deletemid(100);
	if (a == 0)
		cout << "No such element exists." << "\n";
	else
		ll.display();
	cout << "\n";
	a = ll.deletemid(10);
	if (a == 0)
		cout << "No such element exists." << "\n";
	else
		ll.display();
	cout << "\n";
	cout << ll.length();*/
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
