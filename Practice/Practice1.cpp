#include <iostream>

using namespace std;

int queue[5];
int front = -1;
int rear = -1;

void enqueue(int a)
{
    if (rear == 4)
    {
        cout << "Overflow";
        return;
    }
    else
    {
        if (front == -1)
        {
            rear++;
            front++;
        }
        else
        {
            rear++;
        }
        queue[rear] = a;
    }
}

void dequeue()
{
    if (front == rear)
    {
        cout << "Nothing to dequeue";
        return;
    }
    else
    {
        cout << queue[front] << "\n";
        front++;
    }
}

void peek()
{
    cout << queue[front] << "\n";
}

void display()
{
    int traverse = front;
    if (front == -1)
    {
        cout << "underflow";
    }
    else
    {
        while (traverse != (rear + 1))
        {
            cout << queue[traverse] << "\t";
            traverse++;
        }
    }
}

int main()
{
    enqueue(1);
    enqueue(2);
    display();
    cout << "\n";
    dequeue();
    cout << "\n";
    peek();
    enqueue(2);
    enqueue(3);
    enqueue(4);
    enqueue(5);
    cout << "\n";
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
